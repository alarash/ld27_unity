﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	
	public int val = 10;
	public int maxVal = 10;

	
	public void OnHurt(AmountMsg msg){
		val -= msg.amount;
		if(val <= 0){
			val = 0;
			SendMessage("OnDie", msg, SendMessageOptions.DontRequireReceiver);
		}
		
		RisingText.Create(transform.position, "-"+msg.amount.ToString(), Color.red);
	}
	
	public void OnHeal(AmountMsg msg){
		if(val == maxVal) return;
		
		RisingText.Create(transform.position, "+"+msg.amount.ToString(), Color.green);
		
		val += msg.amount;
		if(val > maxVal){
			val = maxVal;
		}
	}
	
	//GUI
	public void GaugeGUI(float x, float y){
		GUIManager.Get().Gauge(new Rect(x, y, 200, 32), "HP", val, maxVal);
		//GUIManager.Get().Gauge(new Rect(x, y + 35, 200, 32), "MP", MP, maxMP);
	}
}
