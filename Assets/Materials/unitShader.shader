﻿
Shader "Stab/Test" {
	Properties {
	 _Color ("Main Color", Color) = (1,1,1,1)
	  _MainTex ("Texture (RGB)", 2D) = "white" {}
	  _SliceGuide ("_SliceGuide (RGB)", 2D) = "white" {}
	  _SliceAmount ("Slice Amount", Range(0.0, 1.0)) = 0.5
	  //New
	  _BumpMap ("Bumpmap", 2D) = "bump" {}
	}
	SubShader {
	  Tags { "RenderType" = "Opaque" }
	  Cull Off
	   //Lighting Off
	  CGPROGRAM
	  #pragma surface surf Lambert addshadow
	  struct Input {
	      float2 uv_MainTex;
	      float2 uv_SliceGuide;
	      float2 uv_BumpMap;
	      float _SliceAmount;
	  };
	  sampler2D _MainTex;
	  sampler2D _SliceGuide;
	  sampler2D _BumpMap;
	  float _SliceAmount;
	 
	  void surf (Input IN, inout SurfaceOutput o) {
	      clip(tex2D (_SliceGuide, IN.uv_SliceGuide).rgb - _SliceAmount);
	      o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
	      o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;  
	 
	  }
	  ENDCG
	}
	Fallback "Diffuse"

}