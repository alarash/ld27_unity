//Maya ASCII 2011 scene
//Name: charBase2.ma
//Last modified: Thu, Sep 19, 2013 01:49:59 PM
//Codeset: 1252
requires maya "2011";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2011";
fileInfo "version" "2011";
fileInfo "cutIdentifier" "201009060019-781618";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.79650794452821827 0.49374609602425756 1.4741531532801109 ;
	setAttr ".r" -type "double3" -5.738352730021103 2186.9999999998636 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1.6918742255687524;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.100000183473 0.030828436765021026 0.0028578232912820438 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 0.58775899023577038;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "Hips";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0 0.13869289285500294 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.13869289285500294 0 1;
	setAttr ".radi" 0.02;
createNode joint -n "L_UpLeg" -p "Hips";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.070009579502510674 -0.0060846385363047539 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.070009579502510674 0.13260825431869819 0 1;
	setAttr ".radi" 0.02;
createNode joint -n "L_Foot" -p "L_UpLeg";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 -0.084689841956147066 -0.0063425146207074061 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.070009579502510674 0.047918412362551122 -0.0063425146207074061 1;
	setAttr ".radi" 0.02;
createNode joint -n "R_UpLeg" -p "Hips";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.070009600000000005 -0.006084892855002938 1.7333369499485123e-033 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0
		 -0.070009600000000005 0.132608 1.7333369499485123e-033 1;
	setAttr ".radi" 0.02;
createNode joint -n "R_Foot" -p "R_UpLeg";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 0.084689600000000004 0.0063425099999999896 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0
		 -0.070009600000000005 0.0479184 -0.00634251 1;
	setAttr ".radi" 0.02;
createNode joint -n "Chest" -p "Hips";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 0.10884257154069879 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.24753546439570173 0 1;
	setAttr ".radi" 0.02;
createNode joint -n "Head" -p "Chest";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 0.10824190228002734 0.013209214932597892 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.35577736667572907 0.013209214932597892 1;
	setAttr ".radi" 0.02;
createNode joint -n "L_Arm" -p "Chest";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.10930592674501809 0.060358703314163709 0 ;
	setAttr ".r" -type "double3" 0 0 -67.482141830679183 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.38297137210461318 -0.92376021139054798 0 0 0.92376021139054798 0.38297137210461318 0 0
		 0 0 1 0 0.10930592674501809 0.30789416770986544 0 1;
	setAttr ".radi" 0.02;
createNode joint -n "L_Hand" -p "L_Arm";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.11544761569239656 2.7755575615628914e-017 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.38297137210461318 -0.92376021139054798 0 0 0.92376021139054798 0.38297137210461318 0 0
		 0 0 1 0 0.15351905853294129 0.20124825383332245 0 1;
	setAttr ".radi" 0.02;
createNode joint -n "R_Arm" -p "Chest";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -0.109306 0.060358535604298269 3.0814879110195774e-033 ;
	setAttr ".r" -type "double3" 0 0 -67.482141830679183 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".bps" -type "matrix" 0.38297137210461318 0.92376021139054798 1.1312799860591169e-016 0
		 0.92376021139054798 -0.38297137210461329 -4.6900466501298443e-017 0 1.2325951644078309e-032 1.2246467991473532e-016 -1 0
		 -0.109306 0.307894 3.0814879110195774e-033 1;
	setAttr ".radi" 0.02;
createNode joint -n "R_Hand" -p "R_Arm";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -0.11544764477881764 1.5472325828880251e-007 -1.3060368254186865e-017 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.38297137210461318 0.92376021139054798 1.1312799860591169e-016 0
		 0.92376021139054798 -0.38297137210461329 -4.6900466501298443e-017 0 1.2325951644078309e-032 1.2246467991473532e-016 -1 0
		 -0.15351899999999999 0.20124800000000001 6.1629758220391547e-033 1;
	setAttr ".radi" 0.02;
createNode transform -n "character";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "characterShape" -p "character";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "characterShapeOrig1" -p "character";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 240 ".uvst[0].uvsp[0:239]" -type "float2" 0.625 0 0.625 0.25 
		0.625 0.5 0.625 0.75 0.625 1 0.875 0 0.875 0.25 0.625 0.625 0.875 0.125 0.625 0.125 
		0.5 0.25 0.5 0.5 0.5 0.625 0.5 0.75 0.5 0 0.5 1 0.5 0.125 0.5 0.375 0.625 0.375 0.75 
		0.25 0.75 0.125 0.625 0.875 0.75 0 0.5 0.875 0.5 0.1875 0.625 0.1875 0.75 0.1875 
		0.625 0.5625 0.875 0.1875 0.5 0.5625 0.5 0.6875 0.625 0.6875 0.875 0.0625 0.75 0.0625 
		0.625 0.0625 0.5 0.0625 0.75 0.1875 0.875 0.1875 0.875 0.25 0.75 0.25 0.75 0 0.875 
		0 0.875 0.0625 0.75 0.0625 0.5 0.375 0.625 0.375 0.625 0.5 0.5 0.5 0.5 0.5625 0.625 
		0.5625 0.5 0.75 0.5 0.6875 0.625 0.6875 0.625 0.75 0.5 0.875 0.625 0.875 0.5 0.0625 
		0.5 0 0.625 0 0.625 0.0625 0.5 0.25 0.5 0.1875 0.625 0.1875 0.625 0.25 0.5 1 0.625 
		1 0.5 0.125 0.625 0.125 0.75 0.125 0.875 0.125 0.5 0.625 0.625 0.625 0.375 0 0.5 
		0 0.5 0.125 0.375 0.125 0.375 0.125 0.5 0.125 0.5 0.25 0.375 0.25 0.5 1 0.375 1 0.375 
		1 0.5 1 0.625 0 0.6875 0 0.6875 0.125 0.625 0.125 0.625 0.9375 0.625 0.9375 0.625 
		1 0.625 1 0.375 0.9375 0.5 0.9375 0.5 1 0.375 1 0.375 0.9375 0.375 0.9375 0.3125 
		0.125 0.3125 0 0.3125 0.25 0.375 0.25 0.5 0.25 0.5 0.3125 0.375 0.3125 0.6875 0.25 
		0.625 0.25 0.375 0.9375 0.5 0.9375 0.5 0.9375 0.375 0.9375 0.625 0.9375 0.625 1 0.625 
		1 0.625 0.9375 0.5 1 0.375 1 0.375 1 0.5 1 0.375 0.125 0.5 0.125 0.5 0.125 0.375 
		0.125 0.625 0.25 0.625 0.125 0.5 0.125 0.375 0.125 0.625 0.125 0.625 0.125 0.375 
		0.125 0.5 0.125 0.75 0 0.75 0.125 0.625 0.875 0.625 0.875 0.375 0.875 0.5 0.875 0.375 
		0.875 0.375 0.875 0.25 0.125 0.25 0 0.25 0.25 0.5 0.375 0.375 0.375 0.75 0.25 0.625 
		1 0.625 0.9375 0.375 1 0.375 0.9375 0.375 0.9375 0.375 1 0.625 0.875 0.625 0.125 
		0.625 0.125 0.625 0.3125 0.625 0.375 0.375 0 0.375 0.125 0.5 0.125 0.5 0 0.375 0.125 
		0.375 0.25 0.5 0.25 0.5 0.125 0.5 1 0.5 1 0.375 1 0.375 1 0.625 0 0.625 0.125 0.6875 
		0.125 0.6875 0 0.625 0.9375 0.625 1 0.625 1 0.625 0.9375 0.375 0.9375 0.375 1 0.5 
		1 0.5 0.9375 0.375 0.9375 0.375 0.9375 0.3125 0.125 0.3125 0 0.3125 0.25 0.375 0.25 
		0.375 0.3125 0.5 0.3125 0.5 0.25 0.625 0.25 0.6875 0.25 0.375 0.9375 0.375 0.9375 
		0.5 0.9375 0.5 0.9375 0.625 0.9375 0.625 0.9375 0.625 1 0.625 1 0.5 1 0.5 1 0.375 
		1 0.375 1 0.375 0.125 0.375 0.125 0.5 0.125 0.5 0.125 0.625 0.125 0.625 0.25 0.375 
		0.125 0.5 0.125 0.625 0.125 0.625 0.125 0.5 0.125 0.375 0.125 0.75 0.125 0.75 0 0.625 
		0.875 0.625 0.875 0.5 0.875 0.375 0.875 0.375 0.875 0.375 0.875 0.25 0.125 0.25 0 
		0.25 0.25 0.5 0.375 0.375 0.375 0.75 0.25 0.625 0.9375 0.625 1 0.375 1 0.375 1 0.375 
		0.9375 0.375 0.9375 0.625 0.875 0.625 0.125 0.625 0.125 0.625 0.3125 0.625 0.375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 176 ".vt";
	setAttr ".vt[0:165]"  0.04540639 0.27792269 0.12134056 0.056884434 0.50678825 
		0.072833709 0.050252546 0.49732685 -0.045829721 0.034504429 0.2782093 0.057860952 
		0.064946212 0.37984449 -0.060487397 0.098134972 0.39255732 0.093670756 1.5453193e-018 
		0.51480395 0.084199943 5.1667794e-018 0.49568909 -0.059062351 0 0.37458882 -0.071333461 
		2.5622544e-018 0.26148042 0.057860952 3.6531191e-018 0.2703107 0.12134056 -2.3991702e-018 
		0.39255732 0.15066943 4.0160566e-018 0.54252958 0.012568796 0.074432418 0.52174807 
		0.012568796 0.11527458 0.3829999 0.028524417 0.05130389 0.27304903 0.089600757 5.2041704e-018 
		0.26589555 0.089600757 -8.0237719e-019 0.4625389 0.14019917 0.086133465 0.46139944 
		0.091179647 0.11053517 0.45993552 0.016736131 0.069648482 0.43770975 -0.070495792 
		4.3190646e-019 0.43456578 -0.078602836 0 0.32358894 -0.01343072 0.074197397 0.32348844 
		-0.008046505 0.091147147 0.32444772 0.055252112 0.082387276 0.33143401 0.091497153 
		2.6070274e-018 0.33143401 0.14536867 -0.11053517 0.45993552 0.016736131 -0.069648482 
		0.43770975 -0.070495792 -0.050252546 0.49732685 -0.045829721 -0.074432418 0.52174807 
		0.012568796 -0.05130389 0.27304903 0.089600757 -0.034504429 0.2782093 0.057860952 
		-0.074197397 0.32348844 -0.008046505 -0.091147147 0.32444772 0.055252112 -0.04540639 
		0.27792269 0.12134056 -0.082387276 0.33143401 0.091497153 -0.086133465 0.46139944 
		0.091179647 -0.056884434 0.50678825 0.072833709 -0.098134972 0.39255732 0.093670756 
		-0.11527458 0.3829999 0.028524417 -0.064946212 0.37984449 -0.060487397 0.099035978 
		0.16272333 0.093774974 0.10735398 0.14875041 -0.071635649 0.086271301 0.33091569 
		0.045840088 0.082807727 0.34720671 -0.049193777 0.10556606 0.24195945 -0.090291828 
		0.10250684 0.24621803 0.073943458 0.10023163 0.090356067 -0.068578355 0.1050628 0.092791006 
		0.082226165 0.058025151 0.24532293 -0.099059813 0.059031803 0.15532103 -0.10330857 
		0.040723402 0.094483085 -0.079295799 0.038548872 0.096658669 0.084022827 0.062663227 
		0.15646999 0.12517884 0.058090381 0.23694593 0.10838406 0.030962868 0.35858133 0.050072562 
		0.037138484 0.35784394 -0.060279142 0.029141298 0 -0.079623111 0.018218484 0 0.079623044 
		0.10096016 0 -0.079623148 0.1118831 0 0.079623006 0.16695178 0.26616091 -0.055109158 
		0.15289438 0.26587722 0.053981066 0.12647173 0.3396627 -0.042859159 0.12364441 0.33947644 
		0.031591527 0.16072737 0.15166569 -0.040012874 0.1514958 0.15199958 0.048936967 0.18403381 
		0.16300017 -0.037998326 0.17917413 0.15748188 0.037150223 -3.1664968e-008 0.25761536 
		-0.10239677 -3.1664968e-008 0.15801713 -0.11624867 -3.1664968e-008 0.09336289 -0.079623073 
		-3.1664968e-008 0.09336289 0.079623073 -3.1664968e-008 0.15461273 0.13803065 -3.1664968e-008 
		0.24195945 0.11200226 -3.1664968e-008 0.35858133 0.050072588 -3.1664968e-008 0.35858133 
		-0.057052333 0.1118831 0.046681445 0.087371595 0.10727546 0.050782517 -0.088271923 
		0.022826005 0.050782517 -0.088271886 0.018218484 0.046681445 0.087371632 0.028577805 
		0.029706843 0.12115115 0.10152377 0.029706843 0.12115111 0.026509315 0 0.13005358 
		0.10359226 0 0.13005355 -3.1664968e-008 0.09336289 4.0489699e-020 0.018218467 0.09336289 
		-3.3527613e-008 0.010727651 0.048731983 0.0031631291 0.010175681 0 -3.3527613e-008 
		0.11992584 0 -7.0780516e-008 0.12427324 0.048731983 0.0031630918 0.11678874 0.09336289 
		-7.0780516e-008 0.11978363 0.1439324 0.014008401 0.11662985 0.19758525 -8.1956387e-008 
		0.14469017 0.14380135 0.0021058938 0.1861546 0.15483861 0.0045056092 0.18411662 0.27189249 
		0.0004989992 0.14103138 0.33694184 -0.0034899637 0.079835057 0.35858133 -0.0034899265 
		0.056152944 0.35858133 -0.0034898985 -3.1664968e-008 0.35858133 -0.0034898724 0.16941296 
		0.20611683 0.048973534 0.19782752 0.21039711 0.00030824737 0.18036957 0.20472538 
		-0.046823107 0.14057471 0.18721429 -0.066205353 0.12468319 0.16109502 -9.1269612e-008 
		0.13108417 0.18541141 0.061564773 -3.1664968e-008 0.11339702 0.12793812 0.05226656 
		0.11847027 0.12204064 0.10229404 0.13086939 0.091499925 0.12585279 0.11864765 0.0070041651 
		0.10559588 0.11883013 -0.074642844 0.054745473 0.1147238 -0.085832231 -3.1664968e-008 
		0.11389385 -0.10295954 -0.099036038 0.16272333 0.093774974 -0.1197837 0.1439324 0.014008401 
		-0.11662991 0.19758525 -8.1956387e-008 -0.10250691 0.24621803 0.073943458 -0.15289444 
		0.26587722 0.053981066 -0.18411668 0.27189249 0.0004989992 -0.14103144 0.33694184 
		-0.0034899637 -0.12364447 0.33947644 0.031591527 -0.1022941 0.13086939 0.091499925 
		-0.12585285 0.11864765 0.0070041651 -0.10735404 0.14875041 -0.071635649 -0.059031866 
		0.15532103 -0.10330857 -0.058025215 0.24532293 -0.099059813 -0.10556613 0.24195945 
		-0.090291828 -0.054745536 0.1147238 -0.085832231 -0.10559595 0.11883013 -0.074642844 
		-0.018218547 -2.118186e-018 0.079623044 -0.010175744 8.6736225e-019 -3.3527613e-008 
		-0.1199259 3.5209718e-020 -7.0780516e-008 -0.11188316 1.648396e-018 0.079623006 -0.062663287 
		0.15646999 0.12517884 -0.052266624 0.11847027 0.12204064 -0.058090445 0.23694593 
		0.10838406 -0.030962931 0.35858133 0.050072562 -0.086271361 0.33091569 0.045840088 
		-0.079835117 0.35858133 -0.0034899265 -0.056153007 0.35858133 -0.0034898985 -0.037138548 
		0.35784394 -0.060279142 -0.082807794 0.34720671 -0.049193777 -0.038548935 0.096658669 
		0.084022827 -0.01821853 0.09336289 -3.3527613e-008 -0.010727715 0.048731983 0.0031631291 
		-0.018218547 0.046681445 0.087371632 -0.040723465 0.094483085 -0.079295799 -0.10023169 
		0.090356067 -0.068578355 -0.10727552 0.050782517 -0.088271923 -0.022826068 0.050782517 
		-0.088271886 -0.1167888 0.09336289 -7.0780516e-008 -0.10506286 0.092791006 0.082226165 
		-0.11188316 0.046681445 0.087371595 -0.1242733 0.048731983 0.0031630918 -0.15149586 
		0.15199958 0.048936967 -0.14469023 0.14380135 0.0021058938 -0.18615466 0.15483861 
		0.0045056092 -0.17917418 0.15748188 0.037150223 -0.12647179 0.3396627 -0.042859159 
		-0.16695184 0.26616091 -0.055109158 -0.12468326 0.16109502 -9.1269612e-008 -0.13108423 
		0.18541141 0.061564773 -0.18036963 0.20472538 -0.046823107 -0.14057477 0.18721429 
		-0.066205353;
	setAttr ".vt[166:175]" -0.16941302 0.20611683 0.048973534 -0.19782758 0.21039711 
		0.00030824737 -0.10096022 1.2759173e-018 -0.079623148 -0.029141361 3.5506331e-018 
		-0.079623111 -0.10152383 0.029706843 0.12115111 -0.028577868 0.029706843 0.12115115 
		-0.026509378 -5.1505228e-019 0.13005358 -0.10359232 2.6229275e-018 0.13005355 -0.16072743 
		0.15166569 -0.040012874 -0.18403387 0.16300017 -0.037998326;
	setAttr -s 344 ".ed";
	setAttr ".ed[0:165]"  0 25 1 1 13 1 2 20 
		1 3 15 1 4 23 1 4 8 1 5 18 
		1 5 14 1 6 1 1 7 2 1 6 12 
		1 7 21 1 9 3 1 8 22 1 10 0 
		1 9 16 1 11 5 1 10 26 1 11 17 
		1 12 7 1 13 2 1 12 13 1 14 4 
		1 13 19 1 15 0 1 14 24 1 16 10 
		1 15 16 1 17 6 1 18 1 1 17 18 
		1 19 14 1 18 19 1 20 4 1 19 20 
		1 21 8 1 20 21 1 22 9 1 23 3 
		1 22 23 1 24 15 1 23 24 1 25 5 
		1 24 25 1 26 11 1 25 26 1 27 28 
		1 29 28 1 30 29 1 30 27 1 32 31 
		1 33 32 1 33 34 1 34 31 1 12 30 
		1 7 29 1 28 21 1 22 33 1 9 32 
		1 31 16 1 10 35 1 35 36 1 36 26 
		1 17 37 1 37 38 1 6 38 1 38 30 
		1 37 27 1 31 35 1 34 36 1 11 39 
		1 39 37 1 39 40 1 27 40 1 40 41 
		1 28 41 1 41 8 1 41 33 1 40 34 
		1 36 39 1 42 93 1 44 99 1 42 47 
		1 43 46 1 44 56 1 45 57 1 46 45 
		1 47 44 1 46 94 1 47 55 1 43 112 
		1 42 110 1 49 92 1 50 46 1 51 43 
		1 50 51 1 52 48 1 51 113 1 53 49 
		1 52 87 1 54 42 1 53 109 1 55 75 
		1 54 55 1 56 76 1 55 56 1 57 77 
		1 56 100 1 57 50 1 52 80 1 53 81 
		1 58 89 1 48 79 1 58 60 1 49 78 
		1 61 90 1 59 61 1 46 62 1 47 63 
		1 62 97 1 45 64 1 62 64 1 44 65 
		1 65 98 1 63 65 1 46 105 1 47 107 
		1 66 95 1 62 104 1 66 68 1 63 102 
		1 68 96 1 67 69 1 70 50 1 71 51 
		1 70 71 1 72 52 1 71 114 1 73 53 
		1 72 86 1 74 54 1 73 108 1 74 75 
		1 75 76 1 76 101 1 77 70 1 78 61 
		1 79 60 1 78 91 1 80 58 1 79 80 
		1 81 59 1 80 88 1 81 78 1 81 82 
		1 78 83 1 82 83 1 59 84 1 82 84 
		1 61 85 1 84 85 1 83 85 1 86 73 
		1 87 53 1 86 87 1 88 81 1;
	setAttr ".ed[166:331]" 87 88 1 89 59 1 88 89 
		1 90 60 1 89 90 1 91 79 1 90 91 
		1 92 48 1 91 92 1 93 43 1 92 111 
		1 94 47 1 93 94 1 95 67 1 94 106 
		1 96 69 1 95 96 1 97 63 1 96 103 
		1 98 64 1 97 98 1 99 45 1 98 99 
		1 100 57 1 99 100 1 101 77 1 100 101 
		1 102 69 1 103 97 1 102 103 1 104 68 
		1 103 104 1 105 66 1 104 105 1 106 95 
		1 105 106 1 107 67 1 106 107 1 107 102 
		1 108 74 1 109 54 1 108 109 1 110 49 
		1 109 110 1 111 93 1 110 111 1 112 48 
		1 111 112 1 113 52 1 112 113 1 114 72 
		1 113 114 1 115 116 1 116 117 1 117 118 
		1 115 118 1 120 119 1 120 121 1 122 121 
		1 119 122 1 115 123 1 123 124 1 124 116 
		1 126 125 1 127 126 1 127 128 1 125 128 
		1 126 129 1 125 130 1 130 129 1 132 131 
		1 132 133 1 134 133 1 131 134 1 135 115 
		1 136 135 1 136 123 1 135 137 1 118 137 
		1 137 138 1 118 139 1 139 138 1 139 140 
		1 140 141 1 138 141 1 142 127 1 143 142 
		1 128 143 1 145 144 1 145 146 1 146 147 
		1 144 147 1 148 149 1 149 150 1 150 151 
		1 148 151 1 153 152 1 153 154 1 154 155 
		1 155 152 1 144 153 1 147 154 1 157 156 
		1 157 158 1 158 159 1 156 159 1 143 160 
		1 161 160 1 128 161 1 139 122 1 121 140 
		1 118 119 1 117 162 1 162 163 1 118 163 
		1 161 164 1 164 165 1 128 165 1 119 166 
		1 166 167 1 167 120 1 163 166 1 71 126 
		1 70 127 1 129 114 1 73 144 1 86 145 
		1 74 135 1 108 136 1 137 75 1 138 76 
		1 141 101 1 142 77 1 154 134 1 133 155 
		1 150 168 1 169 168 1 151 169 1 146 132 
		1 147 131 1 171 170 1 171 172 1 172 173 
		1 170 173 1 147 171 1 154 170 1 131 172 
		1 134 173 1 72 148 1 148 145 1 151 146 
		1 169 132 1 133 168 1 155 150 1 152 149 
		1 116 125 1 124 130 1 128 117 1 165 162 
		1 174 157 1 174 175 1 175 158 1 161 120 
		1 167 164 1 121 160 1 140 143 1;
	setAttr ".ed[332:343]" 141 142 1 166 159 1 158 167 
		1 164 175 1 165 174 1 162 157 1 163 156 
		1 144 136 1 123 153 1 152 124 1 130 149 
		1 129 148 1;
	setAttr -s 172 ".fc[0:171]" -type "polyFaces" 
		f 4 34 -3 -21 23 
		mu 0 4 26 28 6 19 
		f 4 -4 -39 41 40 
		mu 0 4 22 5 32 33 
		f 4 21 20 -10 -20 
		mu 0 4 17 18 2 11 
		f 4 -12 9 2 36 
		mu 0 4 29 11 2 27 
		f 4 -38 39 38 -13 
		mu 0 4 13 30 31 3 
		f 4 -16 12 3 27 
		mu 0 4 23 13 3 21 
		f 4 -18 14 0 45 
		mu 0 4 35 14 0 34 
		f 4 -29 30 29 -9 
		mu 0 4 10 24 25 1 
		f 4 8 1 -22 -11 
		mu 0 4 10 1 18 17 
		f 4 32 -24 -2 -30 
		mu 0 4 25 26 19 1 
		f 4 -25 -41 43 -1 
		mu 0 4 0 22 33 34 
		f 4 -27 -28 24 -15 
		mu 0 4 15 23 21 4 
		f 4 -19 16 6 -31 
		mu 0 4 24 16 9 25 
		f 4 7 -32 -33 -7 
		mu 0 4 9 20 26 25 
		f 4 22 -34 -35 31 
		mu 0 4 20 8 28 26 
		f 4 -36 -37 33 5 
		mu 0 4 12 29 27 7 
		f 4 -14 -6 4 -40 
		mu 0 4 30 12 7 31 
		f 4 -42 -5 -23 25 
		mu 0 4 33 32 8 20 
		f 4 -44 -26 -8 -43 
		mu 0 4 34 33 20 9 
		f 4 -45 -46 42 -17 
		mu 0 4 16 35 34 9 
		f 4 -50 48 47 -47 
		mu 0 4 36 39 38 37 
		f 4 -54 -53 51 50 
		mu 0 4 40 43 42 41 
		f 4 19 55 -49 -55 
		mu 0 4 44 47 46 45 
		f 4 -57 -48 -56 11 
		mu 0 4 48 49 46 47 
		f 4 58 -52 -58 37 
		mu 0 4 50 53 52 51 
		f 4 -60 -51 -59 15 
		mu 0 4 54 55 53 50 
		f 4 -63 -62 -61 17 
		mu 0 4 56 59 58 57 
		f 4 65 -65 -64 28 
		mu 0 4 60 63 62 61 
		f 4 10 54 -67 -66 
		mu 0 4 60 44 45 63 
		f 4 64 66 49 -68 
		mu 0 4 62 63 39 36 
		f 4 61 -70 53 68 
		mu 0 4 58 59 43 40 
		f 4 60 -69 59 26 
		mu 0 4 64 65 55 54 
		f 4 63 -72 -71 18 
		mu 0 4 61 62 67 66 
		f 4 71 67 73 -73 
		mu 0 4 67 62 36 68 
		f 4 -74 46 75 -75 
		mu 0 4 68 36 37 69 
		f 4 -77 -76 56 35 
		mu 0 4 70 71 49 48 
		f 4 57 -78 76 13 
		mu 0 4 51 52 71 70 
		f 4 -79 74 77 52 
		mu 0 4 43 68 69 42 
		f 4 79 72 78 69 
		mu 0 4 59 67 68 43 
		f 4 70 -80 62 44 
		mu 0 4 66 67 59 56 
		f 4 80 178 177 -83 
		mu 0 4 72 73 74 75 
		f 4 -184 186 -124 -125 
		mu 0 4 76 77 78 79 
		f 4 -81 91 211 210 
		mu 0 4 80 81 82 83 
		f 4 -95 -96 93 -84 
		mu 0 4 84 85 86 87 
		f 4 -98 94 90 215 
		mu 0 4 88 89 90 91 
		f 4 -168 170 -116 -117 
		mu 0 4 92 93 94 95 
		f 4 -101 -207 209 -92 
		mu 0 4 81 96 97 82 
		f 4 -104 100 82 89 
		mu 0 4 98 99 72 75 
		f 4 -106 -90 87 84 
		mu 0 4 100 98 75 101 
		f 4 81 190 -108 -85 
		mu 0 4 101 102 103 104 
		f 4 -94 -109 -86 -87 
		mu 0 4 87 86 105 106 
		f 4 -164 166 165 -111 
		mu 0 4 107 108 109 110 
		f 4 96 112 150 -110 
		mu 0 4 111 112 113 114 
		f 4 -93 114 148 174 
		mu 0 4 115 116 117 118 
		f 4 -99 110 153 -115 
		mu 0 4 116 107 110 117 
		f 4 -180 182 181 -133 
		mu 0 4 119 120 121 122 
		f 4 86 120 -122 -118 
		mu 0 4 87 106 123 124 
		f 4 -82 122 123 188 
		mu 0 4 102 101 79 78 
		f 4 -88 118 124 -123 
		mu 0 4 101 75 76 79 
		f 4 -178 180 203 -127 
		mu 0 4 75 74 125 126 
		f 4 117 128 199 -126 
		mu 0 4 87 124 127 128 
		f 4 183 130 195 194 
		mu 0 4 77 76 129 130 
		f 4 -119 126 204 -131 
		mu 0 4 76 75 126 129 
		f 4 -135 -136 133 95 
		mu 0 4 85 131 132 86 
		f 4 217 -138 134 97 
		mu 0 4 88 133 134 89 
		f 4 -139 -163 164 163 
		mu 0 4 107 135 136 108 
		f 4 -141 -206 207 206 
		mu 0 4 96 137 138 97 
		f 4 -143 140 103 102 
		mu 0 4 139 140 99 98 
		f 4 -144 -103 105 104 
		mu 0 4 141 139 98 100 
		f 4 -145 -105 107 192 
		mu 0 4 142 143 104 103 
		f 4 -134 -146 -107 108 
		mu 0 4 86 132 144 105 
		f 4 -149 146 115 172 
		mu 0 4 118 117 95 94 
		f 4 -151 147 -114 -150 
		mu 0 4 114 113 145 146 
		f 4 -166 168 167 -152 
		mu 0 4 110 109 93 92 
		f 4 -157 158 160 -162 
		mu 0 4 147 148 149 150 
		f 4 -154 154 156 -156 
		mu 0 4 117 110 148 147 
		f 4 151 157 -159 -155 
		mu 0 4 110 92 149 148 
		f 4 116 159 -161 -158 
		mu 0 4 92 95 150 149 
		f 4 -147 155 161 -160 
		mu 0 4 95 117 147 150 
		f 4 -165 -140 136 99 
		mu 0 4 108 136 151 111 
		f 4 -167 -100 109 152 
		mu 0 4 109 108 111 114 
		f 4 -169 -153 149 111 
		mu 0 4 93 109 114 146 
		f 4 -171 -112 113 -170 
		mu 0 4 94 93 146 145 
		f 4 -172 -173 169 -148 
		mu 0 4 113 118 94 145 
		f 4 -174 -175 171 -113 
		mu 0 4 112 115 118 113 
		f 4 -176 -211 213 -91 
		mu 0 4 90 80 83 91 
		f 4 -179 175 83 88 
		mu 0 4 74 73 84 87 
		f 4 -181 -89 125 201 
		mu 0 4 125 74 87 128 
		f 4 -183 -128 129 131 
		mu 0 4 121 120 152 153 
		f 4 119 -195 197 -129 
		mu 0 4 124 77 130 127 
		f 4 -187 -120 121 -186 
		mu 0 4 78 77 124 123 
		f 4 -188 -189 185 -121 
		mu 0 4 106 102 78 123 
		f 4 -191 187 85 -190 
		mu 0 4 103 102 106 154 
		f 4 -192 -193 189 106 
		mu 0 4 155 142 103 154 
		f 4 -196 193 -182 184 
		mu 0 4 130 129 122 121 
		f 4 -198 -185 -132 -197 
		mu 0 4 127 130 121 153 
		f 4 -200 196 -130 -199 
		mu 0 4 128 127 153 152 
		f 4 -201 -202 198 127 
		mu 0 4 120 125 128 152 
		f 4 -204 200 179 -203 
		mu 0 4 126 125 120 119 
		f 4 -205 202 132 -194 
		mu 0 4 129 126 119 122 
		f 4 -208 -142 138 101 
		mu 0 4 97 138 135 107 
		f 4 -210 -102 98 -209 
		mu 0 4 82 97 107 116 
		f 4 -212 208 92 176 
		mu 0 4 83 82 116 115 
		f 4 -214 -177 173 -213 
		mu 0 4 91 83 115 112 
		f 4 -215 -216 212 -97 
		mu 0 4 111 88 91 112 
		f 4 -137 -217 -218 214 
		mu 0 4 111 151 133 88 
		f 4 221 -221 -220 -219 
		mu 0 4 156 157 158 159 
		f 4 225 224 -224 222 
		mu 0 4 160 161 162 163 
		f 4 -229 -228 -227 218 
		mu 0 4 164 165 166 167 
		f 4 232 -232 230 229 
		mu 0 4 168 169 170 171 
		f 4 -236 -235 -230 233 
		mu 0 4 172 173 174 175 
		f 4 239 238 -238 236 
		mu 0 4 176 177 178 179 
		f 4 226 -243 241 240 
		mu 0 4 167 166 180 181 
		f 4 -245 -222 -241 243 
		mu 0 4 182 157 156 183 
		f 4 -248 -247 244 245 
		mu 0 4 184 185 157 182 
		f 4 247 250 -250 -249 
		mu 0 4 185 186 187 188 
		f 4 253 252 251 231 
		mu 0 4 169 189 190 170 
		f 4 257 -257 -256 254 
		mu 0 4 191 192 193 194 
		f 4 261 -261 -260 -259 
		mu 0 4 195 196 197 198 
		f 4 -266 -265 -264 262 
		mu 0 4 199 200 201 202 
		f 4 263 -268 -258 266 
		mu 0 4 202 201 192 191 
		f 4 271 -271 -270 268 
		mu 0 4 203 204 205 206 
		f 4 274 273 -273 -254 
		mu 0 4 169 207 208 189 
		f 4 -277 -225 -276 248 
		mu 0 4 188 162 161 185 
		f 4 275 -226 -278 246 
		mu 0 4 185 161 160 157 
		f 4 280 -280 -279 220 
		mu 0 4 157 209 210 158 
		f 4 283 -283 -282 -275 
		mu 0 4 169 211 212 207 
		f 4 -287 -286 -285 -223 
		mu 0 4 163 213 214 160 
		f 4 284 -288 -281 277 
		mu 0 4 160 214 209 157 
		f 4 -231 -290 135 288 
		mu 0 4 171 170 215 216 
		f 4 -234 -289 137 -291 
		mu 0 4 172 175 217 218 
		f 4 -255 -293 162 291 
		mu 0 4 191 194 219 220 
		f 4 -242 -295 205 293 
		mu 0 4 181 180 221 222 
		f 4 -296 -244 -294 142 
		mu 0 4 223 182 183 224 
		f 4 -297 -246 295 143 
		mu 0 4 225 184 182 223 
		f 4 -298 -251 296 144 
		mu 0 4 226 187 186 227 
		f 4 -252 298 145 289 
		mu 0 4 170 190 228 215 
		f 4 -301 -239 -300 264 
		mu 0 4 200 178 177 201 
		f 4 303 302 -302 260 
		mu 0 4 196 229 230 197 
		f 4 305 -237 -305 256 
		mu 0 4 192 176 179 193 
		f 4 309 -309 -308 306 
		mu 0 4 231 232 233 234 
		f 4 311 -307 -311 267 
		mu 0 4 201 231 234 192 
		f 4 310 307 -313 -306 
		mu 0 4 192 234 233 176 
		f 4 312 308 -314 -240 
		mu 0 4 176 233 232 177 
		f 4 313 -310 -312 299 
		mu 0 4 177 232 231 201 
		f 4 -316 -315 139 292 
		mu 0 4 194 195 235 219 
		f 4 -317 -262 315 255 
		mu 0 4 193 196 195 194 
		f 4 -318 -304 316 304 
		mu 0 4 179 229 196 193 
		f 4 318 -303 317 237 
		mu 0 4 178 230 229 179 
		f 4 301 -319 300 319 
		mu 0 4 197 230 178 200 
		f 4 259 -320 265 320 
		mu 0 4 198 197 200 199 
		f 4 234 -323 228 321 
		mu 0 4 174 173 165 164 
		f 4 -324 -233 -322 219 
		mu 0 4 158 169 168 159 
		f 4 -325 -284 323 278 
		mu 0 4 210 211 169 158 
		f 4 -328 -327 325 269 
		mu 0 4 205 236 237 206 
		f 4 281 -330 286 -329 
		mu 0 4 207 212 213 163 
		f 4 330 -274 328 223 
		mu 0 4 162 208 207 163 
		f 4 272 -331 276 331 
		mu 0 4 189 208 162 188 
		f 4 332 -253 -332 249 
		mu 0 4 187 238 189 188 
		f 4 -299 -333 297 191 
		mu 0 4 239 238 187 226 
		f 4 -335 270 -334 285 
		mu 0 4 213 205 204 214 
		f 4 335 327 334 329 
		mu 0 4 212 236 205 213 
		f 4 336 326 -336 282 
		mu 0 4 211 237 236 212 
		f 4 -326 -337 324 337 
		mu 0 4 206 237 211 210 
		f 4 338 -269 -338 279 
		mu 0 4 209 203 206 210 
		f 4 333 -272 -339 287 
		mu 0 4 214 204 203 209 
		f 4 -340 -292 141 294 
		mu 0 4 180 191 220 221 
		f 4 340 -267 339 242 
		mu 0 4 166 202 191 180 
		f 4 -342 -263 -341 227 
		mu 0 4 165 199 202 166 
		f 4 342 -321 341 322 
		mu 0 4 173 198 199 165 
		f 4 258 -343 235 343 
		mu 0 4 195 198 173 172 
		f 4 -344 290 216 314 
		mu 0 4 195 172 218 235 ;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n"
		+ "            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n"
		+ "            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n"
		+ "                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n"
		+ "                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n"
		+ "            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n"
		+ "                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n"
		+ "                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 1\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n"
		+ "                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 1\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 0\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n"
		+ "            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n"
		+ "                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n"
		+ "                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 21 100 -ps 2 79 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 1\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 0\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 1\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 0\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 100 -ast 1 -aet 100 ";
	setAttr ".st" 6;
createNode skinCluster -n "skinCluster1";
	setAttr -s 176 ".wl";
	setAttr ".wl[0].w[6]"  1;
	setAttr ".wl[1].w[6]"  1;
	setAttr ".wl[2].w[6]"  1;
	setAttr ".wl[3].w[6]"  1;
	setAttr ".wl[4].w[6]"  1;
	setAttr ".wl[5].w[6]"  1;
	setAttr ".wl[6].w[6]"  1;
	setAttr ".wl[7].w[6]"  1;
	setAttr ".wl[8].w[6]"  1;
	setAttr ".wl[9].w[6]"  1;
	setAttr ".wl[10].w[6]"  1;
	setAttr ".wl[11].w[6]"  1;
	setAttr ".wl[12].w[6]"  1;
	setAttr ".wl[13].w[6]"  1;
	setAttr ".wl[14].w[6]"  1;
	setAttr ".wl[15].w[6]"  1;
	setAttr ".wl[16].w[6]"  1;
	setAttr ".wl[17].w[6]"  1;
	setAttr ".wl[18].w[6]"  1;
	setAttr ".wl[19].w[6]"  1;
	setAttr ".wl[20].w[6]"  1;
	setAttr ".wl[21].w[6]"  1;
	setAttr ".wl[22].w[6]"  1;
	setAttr ".wl[23].w[6]"  1;
	setAttr ".wl[24].w[6]"  1;
	setAttr ".wl[25].w[6]"  1;
	setAttr ".wl[26].w[6]"  1;
	setAttr ".wl[27].w[6]"  1;
	setAttr ".wl[28].w[6]"  1;
	setAttr ".wl[29].w[6]"  1;
	setAttr ".wl[30].w[6]"  1;
	setAttr ".wl[31].w[6]"  1;
	setAttr ".wl[32].w[6]"  1;
	setAttr ".wl[33].w[6]"  1;
	setAttr ".wl[34].w[6]"  1;
	setAttr ".wl[35].w[6]"  1;
	setAttr ".wl[36].w[6]"  1;
	setAttr ".wl[37].w[6]"  1;
	setAttr ".wl[38].w[6]"  1;
	setAttr ".wl[39].w[6]"  1;
	setAttr ".wl[40].w[6]"  1;
	setAttr ".wl[41].w[6]"  1;
	setAttr -s 2 ".wl[42].w";
	setAttr ".wl[42].w[0]" 0.76700000464916229;
	setAttr ".wl[42].w[5]" 0.23299999535083771;
	setAttr -s 2 ".wl[43].w";
	setAttr ".wl[43].w[0]" 0.76700000464916229;
	setAttr ".wl[43].w[5]" 0.23299999535083771;
	setAttr ".wl[44].w[5]"  1;
	setAttr ".wl[45].w[5]"  1;
	setAttr -s 2 ".wl[46].w";
	setAttr ".wl[46].w[5]" 0.8736000657081604;
	setAttr ".wl[46].w[7]" 0.1263999342918396;
	setAttr -s 2 ".wl[47].w";
	setAttr ".wl[47].w[5]" 0.8736000657081604;
	setAttr ".wl[47].w[7]" 0.1263999342918396;
	setAttr ".wl[48].w[1]"  1;
	setAttr ".wl[49].w[1]"  1;
	setAttr ".wl[50].w[5]"  1;
	setAttr -s 2 ".wl[51].w";
	setAttr ".wl[51].w[0]" 0.76700000464916229;
	setAttr ".wl[51].w[5]" 0.23299999535083771;
	setAttr ".wl[52].w[1]"  1;
	setAttr ".wl[53].w[1]"  1;
	setAttr -s 2 ".wl[54].w";
	setAttr ".wl[54].w[0]" 0.76700000464916229;
	setAttr ".wl[54].w[5]" 0.23299999535083771;
	setAttr ".wl[55].w[5]"  1;
	setAttr ".wl[56].w[5]"  1;
	setAttr ".wl[57].w[5]"  1;
	setAttr ".wl[58].w[2]"  1;
	setAttr ".wl[59].w[2]"  1;
	setAttr ".wl[60].w[2]"  1;
	setAttr ".wl[61].w[2]"  1;
	setAttr ".wl[62].w[7]"  1;
	setAttr ".wl[63].w[7]"  1;
	setAttr -s 2 ".wl[64].w";
	setAttr ".wl[64].w[5]" 0.58240002393722534;
	setAttr ".wl[64].w[7]" 0.41759997606277466;
	setAttr -s 2 ".wl[65].w";
	setAttr ".wl[65].w[5]" 0.58240002393722534;
	setAttr ".wl[65].w[7]" 0.41759997606277466;
	setAttr ".wl[66].w[8]"  1;
	setAttr ".wl[67].w[8]"  1;
	setAttr ".wl[68].w[8]"  1;
	setAttr ".wl[69].w[8]"  1;
	setAttr ".wl[70].w[5]"  1;
	setAttr -s 2 ".wl[71].w";
	setAttr ".wl[71].w[0]" 0.76700000464916229;
	setAttr ".wl[71].w[5]" 0.23299999535083771;
	setAttr ".wl[72].w[0]"  1;
	setAttr ".wl[73].w[0]"  1;
	setAttr -s 2 ".wl[74].w";
	setAttr ".wl[74].w[0]" 0.76700000464916229;
	setAttr ".wl[74].w[5]" 0.23299999535083771;
	setAttr ".wl[75].w[5]"  1;
	setAttr ".wl[76].w[5]"  1;
	setAttr ".wl[77].w[5]"  1;
	setAttr ".wl[78].w[2]"  1;
	setAttr ".wl[79].w[2]"  1;
	setAttr ".wl[80].w[2]"  1;
	setAttr ".wl[81].w[2]"  1;
	setAttr ".wl[82].w[2]"  1;
	setAttr ".wl[83].w[2]"  1;
	setAttr ".wl[84].w[2]"  1;
	setAttr ".wl[85].w[2]"  1;
	setAttr ".wl[86].w[0]"  1;
	setAttr ".wl[87].w[1]"  1;
	setAttr ".wl[88].w[2]"  1;
	setAttr ".wl[89].w[2]"  1;
	setAttr ".wl[90].w[2]"  1;
	setAttr ".wl[91].w[2]"  1;
	setAttr ".wl[92].w[1]"  1;
	setAttr -s 2 ".wl[93].w";
	setAttr ".wl[93].w[0]" 0.76700000464916229;
	setAttr ".wl[93].w[5]" 0.23299999535083771;
	setAttr ".wl[94].w[0]"  1;
	setAttr ".wl[95].w[8]"  1;
	setAttr ".wl[96].w[8]"  1;
	setAttr ".wl[97].w[7]"  1;
	setAttr -s 2 ".wl[98].w";
	setAttr ".wl[98].w[5]" 0.58240002393722534;
	setAttr ".wl[98].w[7]" 0.41759997606277466;
	setAttr ".wl[99].w[5]"  1;
	setAttr ".wl[100].w[5]"  1;
	setAttr ".wl[101].w[5]"  1;
	setAttr ".wl[102].w[8]"  1;
	setAttr ".wl[103].w[8]"  1;
	setAttr ".wl[104].w[8]"  1;
	setAttr ".wl[105].w[8]"  1;
	setAttr ".wl[106].w[8]"  1;
	setAttr ".wl[107].w[8]"  1;
	setAttr ".wl[108].w[0]"  1;
	setAttr ".wl[109].w[0]"  1;
	setAttr -s 2 ".wl[110].w[0:1]"  0.40000000596046448 0.59999999403953552;
	setAttr -s 2 ".wl[111].w[0:1]"  0.30000001192092896 0.69999998807907104;
	setAttr -s 2 ".wl[112].w[0:1]"  0.30000001192092896 0.69999998807907104;
	setAttr ".wl[113].w[0]"  1;
	setAttr ".wl[114].w[0]"  1;
	setAttr -s 2 ".wl[115].w";
	setAttr ".wl[115].w[0]" 0.76700000464916229;
	setAttr ".wl[115].w[5]" 0.23299999535083771;
	setAttr -s 2 ".wl[116].w";
	setAttr ".wl[116].w[0]" 0.76700000464916229;
	setAttr ".wl[116].w[5]" 0.23299999535083771;
	setAttr -s 2 ".wl[117].w";
	setAttr ".wl[117].w[0]" 0.99999970197677612;
	setAttr ".wl[117].w[10]" 2.9802322387695313e-007;
	setAttr -s 3 ".wl[118].w";
	setAttr ".wl[118].w[5]" 0.87359949293132289;
	setAttr ".wl[118].w[9:10]" 0.12639985141758459 6.5565109252929688e-007;
	setAttr -s 2 ".wl[119].w[9:10]"  0.9999997615814209 2.384185791015625e-007;
	setAttr ".wl[120].w[9]"  1;
	setAttr -s 2 ".wl[121].w";
	setAttr ".wl[121].w[5]" 0.58240002393722534;
	setAttr ".wl[121].w[9]" 0.41759997606277466;
	setAttr -s 2 ".wl[122].w";
	setAttr ".wl[122].w[5]" 0.58239987403741644;
	setAttr ".wl[122].w[9]" 0.4176001259625835;
	setAttr -s 2 ".wl[123].w";
	setAttr ".wl[123].w[0]" 0.39999998807907211;
	setAttr ".wl[123].w[3]" 0.60000001192092789;
	setAttr -s 2 ".wl[124].w";
	setAttr ".wl[124].w[0]" 0.30000001192092896;
	setAttr ".wl[124].w[3]" 0.69999998807907104;
	setAttr -s 2 ".wl[125].w";
	setAttr ".wl[125].w[0]" 0.76700000464916229;
	setAttr ".wl[125].w[5]" 0.23299999535083771;
	setAttr -s 2 ".wl[126].w";
	setAttr ".wl[126].w[0]" 0.76700000464916229;
	setAttr ".wl[126].w[5]" 0.23299999535083771;
	setAttr ".wl[127].w[5]"  1;
	setAttr -s 3 ".wl[128].w";
	setAttr ".wl[128].w[5]" 0.87359963870966195;
	setAttr ".wl[128].w[9:10]" 0.12639987251004234 4.8878029572872073e-007;
	setAttr ".wl[129].w[0]"  1;
	setAttr -s 2 ".wl[130].w";
	setAttr ".wl[130].w[0]" 0.30000001192092896;
	setAttr ".wl[130].w[3]" 0.69999998807907104;
	setAttr ".wl[131].w[4]"  1;
	setAttr ".wl[132].w[4]"  1;
	setAttr ".wl[133].w[4]"  1;
	setAttr ".wl[134].w[4]"  1;
	setAttr -s 2 ".wl[135].w";
	setAttr ".wl[135].w[0]" 0.76700000464916229;
	setAttr ".wl[135].w[5]" 0.23299999535083771;
	setAttr -s 2 ".wl[136].w";
	setAttr ".wl[136].w[0]" 0.99999934434890747;
	setAttr ".wl[136].w[3]" 6.5565109252929688e-007;
	setAttr ".wl[137].w[5]"  1;
	setAttr ".wl[138].w[5]"  1;
	setAttr -s 2 ".wl[139].w";
	setAttr ".wl[139].w[5]" 0.99999931874958681;
	setAttr ".wl[139].w[9]" 6.8125039897705463e-007;
	setAttr ".wl[140].w[5]"  1;
	setAttr ".wl[141].w[5]"  1;
	setAttr ".wl[142].w[5]"  1;
	setAttr ".wl[143].w[5]"  1;
	setAttr -s 2 ".wl[144].w";
	setAttr ".wl[144].w[0]" 7.4933366651990433e-008;
	setAttr ".wl[144].w[3]" 0.99999992506663338;
	setAttr ".wl[145].w[3]"  1;
	setAttr ".wl[146].w[4]"  1;
	setAttr ".wl[147].w[4]"  1;
	setAttr -s 2 ".wl[148].w";
	setAttr ".wl[148].w[0]" 1.3709068298339844e-006;
	setAttr ".wl[148].w[3]" 0.99999862909317017;
	setAttr -s 2 ".wl[149].w";
	setAttr ".wl[149].w[0]" 1.2516975900211946e-007;
	setAttr ".wl[149].w[3]" 0.999999874830241;
	setAttr ".wl[150].w[4]"  1;
	setAttr ".wl[151].w[4]"  1;
	setAttr ".wl[152].w[3]"  1;
	setAttr -s 2 ".wl[153].w";
	setAttr ".wl[153].w[0]" 5.364418242947977e-008;
	setAttr ".wl[153].w[3]" 0.99999994635581757;
	setAttr ".wl[154].w[4]"  1;
	setAttr ".wl[155].w[4]"  1;
	setAttr ".wl[156].w[10]"  1;
	setAttr ".wl[157].w[10]"  1;
	setAttr ".wl[158].w[10]"  1;
	setAttr ".wl[159].w[10]"  1;
	setAttr -s 2 ".wl[160].w";
	setAttr ".wl[160].w[5]" 0.58240002393722534;
	setAttr ".wl[160].w[9]" 0.41759997606277466;
	setAttr ".wl[161].w[9]"  1;
	setAttr ".wl[162].w[10]"  1;
	setAttr ".wl[163].w[10]"  1;
	setAttr ".wl[164].w[10]"  1;
	setAttr ".wl[165].w[10]"  1;
	setAttr ".wl[166].w[10]"  1;
	setAttr ".wl[167].w[10]"  1;
	setAttr ".wl[168].w[4]"  1;
	setAttr ".wl[169].w[4]"  1;
	setAttr ".wl[170].w[4]"  1;
	setAttr ".wl[171].w[4]"  1;
	setAttr ".wl[172].w[4]"  1;
	setAttr ".wl[173].w[4]"  1;
	setAttr ".wl[174].w[10]"  1;
	setAttr ".wl[175].w[10]"  1;
	setAttr -s 11 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -0.13869289285500294 0 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.070009579502510674 -0.13260825431869819 0 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.070009579502510674 -0.047918412362551122 0.0063425146207074061 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0
		 0.070009600000000005 0.132608 -1.6239796274133218e-017 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0
		 0.070009600000000005 0.0479184 -0.0063425100000000061 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -0.24753546439570173 0 1;
	setAttr ".pm[6]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -0.35577736667572907 -0.013209214932597892 1;
	setAttr ".pm[7]" -type "matrix" 0.38297137210461318 0.92376021139054798 0 0 -0.92376021139054798 0.38297137210461318 0 0
		 0 0 1 0 0.24255934070487623 -0.21888711786707271 0 1;
	setAttr ".pm[8]" -type "matrix" 0.38297137210461318 0.92376021139054798 0 0 -0.92376021139054798 0.38297137210461318 0 0
		 0 0 1 0 0.12711172501247966 -0.21888711786707271 0 1;
	setAttr ".pm[9]" -type "matrix" 0.38297137210461329 0.92376021139054798 1.8488927466117464e-032 0
		 0.92376021139054798 -0.38297137210461318 1.224646799147353e-016 0 1.1312799860591169e-016 -4.6900466501298437e-017 -1 0
		 -0.24255915772661452 0.21888712130903301 -3.7706140157667508e-017 1;
	setAttr ".pm[10]" -type "matrix" 0.38297137210461329 0.92376021139054798 1.8488927466117464e-032 0
		 0.92376021139054798 -0.38297137210461318 1.224646799147353e-016 0 1.1312799860591169e-016 -4.6900466501298437e-017 -1 0
		 -0.12711151294779688 0.21888696658577472 -2.4645771903480643e-017 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 11 ".ma";
	setAttr -s 11 ".dpf[0:10]"  4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 11 ".lw";
	setAttr -s 11 ".lw";
	setAttr ".mi" 4;
	setAttr ".ptw" -type "doubleArray" 176 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.23299999535083771 0.23299999535083771
		 1 1 0.8736000657081604 0.8736000657081604 0 0 1 0.23299999535083771 0 0 0.23299999535083771
		 1 1 1 0 0 0 0 0 0 0.58240002393722534 0.58240002393722534 0 0 0 0 1 0.23299999535083771
		 0 0 0.23299999535083771 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.23299999535083771 0
		 0 0 0 0.58240002393722534 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0.23299999535083771 0.23299999535083771
		 0 0.87359949293132289 0 0 0.58240002393722534 0.58239987403741644 0 0 0.23299999535083771
		 0.23299999535083771 1 0.87359963870966195 0 0 0 0 0 0 0.23299999535083771 0 1 1 0.99999931874958681
		 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.58240002393722534 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".ucm" yes;
createNode tweak -n "tweak1";
	setAttr -s 16 ".vl[0].vt";
	setAttr ".vl[0].vt[79:80]" -type "float3" 0 0 0.0068041314  0 0 0.0068041314 ;
	setAttr ".vl[0].vt[82:85]" -type "float3" 0 0.010368309 0.0043275389  0 
		0.010368309 0.0043275389  0 0 0.011247316  0 0 0.011247316 ;
	setAttr ".vl[0].vt[110:112]" -type "float3" 0 0 0  0 0 0  0 0 0 ;
	setAttr ".vl[0].vt[150:151]" -type "float3" 0 0 0.0068041314  0 0 0.0068041314 ;
	setAttr ".vl[0].vt[170:173]" -type "float3" 0 0.010368309 0.0043275389  0 
		0.010368309 0.0043275389  0 0 0.011247316  0 0 0.011247316 ;
createNode objectSet -n "skinCluster1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	setAttr -s 11 ".wm";
	setAttr -s 11 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0.13869289285500294
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.070009579502510674 -0.0060846385363047539
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 -0.084689841956147066
		 -0.0063425146207074061 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.070009600000000005
		 -0.006084892855002938 1.7333369499485123e-033 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-1 0 0 6.123233995736766e-017 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0.084689600000000004
		 0.0063425099999999896 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0.10884257154069879
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0.10824190228002734
		 0.013209214932597892 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 -1.1777855612431456 0 0.10930592674501809
		 0.060358703314163709 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.11544761569239656 2.7755575615628914e-017
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 -1.1777855612431456 0 -0.109306
		 0.060358535604298269 3.0814879110195774e-033 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-1 0 0 6.123233995736766e-017 1 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.11544764477881764
		 1.5472325828880251e-007 -1.3060368254186865e-017 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0 1 1 1 1 yes;
	setAttr -s 11 ".m";
	setAttr -s 11 ".p";
	setAttr ".bp" yes;
createNode groupId -n "groupId4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:171]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :renderGlobalsList1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "Hips.s" "L_UpLeg.is";
connectAttr "Hips.s" "Chest.is";
connectAttr "L_Arm.s" "L_Hand.is";
connectAttr "Chest.s" "R_Arm.is";
connectAttr "R_Arm.s" "R_Hand.is";
connectAttr "groupParts4.og" "characterShape.i";
connectAttr "groupId4.id" "characterShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "characterShape.iog.og[0].gco";
connectAttr "skinCluster1GroupId.id" "characterShape.iog.og[1].gid";
connectAttr "skinCluster1Set.mwc" "characterShape.iog.og[1].gco";
connectAttr "groupId3.id" "characterShape.iog.og[2].gid";
connectAttr "tweakSet1.mwc" "characterShape.iog.og[2].gco";
connectAttr "tweak1.vl[0].vt[0]" "characterShape.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "Hips.wm" "skinCluster1.ma[0]";
connectAttr "L_UpLeg.wm" "skinCluster1.ma[1]";
connectAttr "L_Foot.wm" "skinCluster1.ma[2]";
connectAttr "R_UpLeg.wm" "skinCluster1.ma[3]";
connectAttr "R_Foot.wm" "skinCluster1.ma[4]";
connectAttr "Chest.wm" "skinCluster1.ma[5]";
connectAttr "Head.wm" "skinCluster1.ma[6]";
connectAttr "L_Arm.wm" "skinCluster1.ma[7]";
connectAttr "L_Hand.wm" "skinCluster1.ma[8]";
connectAttr "R_Arm.wm" "skinCluster1.ma[9]";
connectAttr "R_Hand.wm" "skinCluster1.ma[10]";
connectAttr "Hips.liw" "skinCluster1.lw[0]";
connectAttr "L_UpLeg.liw" "skinCluster1.lw[1]";
connectAttr "L_Foot.liw" "skinCluster1.lw[2]";
connectAttr "R_UpLeg.liw" "skinCluster1.lw[3]";
connectAttr "R_Foot.liw" "skinCluster1.lw[4]";
connectAttr "Chest.liw" "skinCluster1.lw[5]";
connectAttr "Head.liw" "skinCluster1.lw[6]";
connectAttr "L_Arm.liw" "skinCluster1.lw[7]";
connectAttr "L_Hand.liw" "skinCluster1.lw[8]";
connectAttr "R_Arm.liw" "skinCluster1.lw[9]";
connectAttr "R_Hand.liw" "skinCluster1.lw[10]";
connectAttr "Chest.msg" "skinCluster1.ptt";
connectAttr "groupParts3.og" "tweak1.ip[0].ig";
connectAttr "groupId3.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "characterShape.iog.og[1]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId3.msg" "tweakSet1.gn" -na;
connectAttr "characterShape.iog.og[2]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "characterShapeOrig1.w" "groupParts3.ig";
connectAttr "groupId3.id" "groupParts3.gi";
connectAttr "Hips.msg" "bindPose1.m[0]";
connectAttr "L_UpLeg.msg" "bindPose1.m[1]";
connectAttr "L_Foot.msg" "bindPose1.m[2]";
connectAttr "R_UpLeg.msg" "bindPose1.m[3]";
connectAttr "R_Foot.msg" "bindPose1.m[4]";
connectAttr "Chest.msg" "bindPose1.m[5]";
connectAttr "Head.msg" "bindPose1.m[6]";
connectAttr "L_Arm.msg" "bindPose1.m[7]";
connectAttr "L_Hand.msg" "bindPose1.m[8]";
connectAttr "R_Arm.msg" "bindPose1.m[9]";
connectAttr "R_Hand.msg" "bindPose1.m[10]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[0]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[0]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[5]" "bindPose1.p[7]";
connectAttr "bindPose1.m[7]" "bindPose1.p[8]";
connectAttr "bindPose1.m[5]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "Hips.bps" "bindPose1.wm[0]";
connectAttr "L_UpLeg.bps" "bindPose1.wm[1]";
connectAttr "L_Foot.bps" "bindPose1.wm[2]";
connectAttr "R_UpLeg.bps" "bindPose1.wm[3]";
connectAttr "R_Foot.bps" "bindPose1.wm[4]";
connectAttr "Chest.bps" "bindPose1.wm[5]";
connectAttr "Head.bps" "bindPose1.wm[6]";
connectAttr "L_Arm.bps" "bindPose1.wm[7]";
connectAttr "L_Hand.bps" "bindPose1.wm[8]";
connectAttr "R_Arm.bps" "bindPose1.wm[9]";
connectAttr "R_Hand.bps" "bindPose1.wm[10]";
connectAttr "skinCluster1.og[0]" "groupParts4.ig";
connectAttr "groupId4.id" "groupParts4.gi";
connectAttr "characterShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
// End of charBase2.ma
