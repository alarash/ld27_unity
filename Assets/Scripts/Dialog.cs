﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DialogLine {
	public string text;
	public DialogChoice[] choices;
	
}

[System.Serializable]
public class DialogChoice {
	public string text;
	public int gotoLine;
	
	public DialogChoice(string text, int gotoLine){
		this.text = text;
		this.gotoLine = gotoLine;
	}
	
	public static DialogChoice defaultQuit = new DialogChoice("Bye now", -1);
}

public class Dialog : MonoBehaviour {
	
	private bool _opened = false;
	
	public string header = "Dialog";
	public DialogLine[] lines;
	private int _currentLine = 0;
	
	private const int MARGIN = 10;
	private const int WIDTH = 300;
	private const int HEIGHT = 200;
	
	public void Open(){
		_opened = true;
		GetComponent<Unit>().block = true;
		_currentLine = 0;
	}
	
	public void Close(){
		_opened = false;
		GetComponent<Unit>().block = false;
	}
	
	public void OnEnter(Unit unit){
		Open();
	}
	
	public void OnLeave(Unit unit){
		Close();
	}
	
	void OnGUI(){
		if(!_opened) return;
		
		GUI.skin = GUIManager.instance.guiSkin;
		//GUI.Box(new Rect(0, Screen.height - 100, Screen.width, 100), "Dialog");
		
		GUI.BeginGroup(new Rect(Screen.width - WIDTH - 5, 5, WIDTH, HEIGHT));
		GUI.Box(new Rect(0, 0, WIDTH, HEIGHT), header);
		
			GUI.BeginGroup(new Rect(MARGIN, MARGIN+15, WIDTH - MARGIN*2, HEIGHT-MARGIN*2-15));
		
			GUILine(lines[_currentLine]);
			
			GUI.EndGroup();
		
		/*
		//if(GUI.Button(new Rect(5, 300 - 5 - 20, 300 - 10, 20), "Bye now")){
		if(GUI.Button(new Rect(5, 200-5-30, 300-10, 30), "Bye now")){
			_opened = false;
			GetComponent<Unit>().block = false;
		}*/
		
		GUI.EndGroup();
	}
	
	void GUILine(DialogLine line){
		
		//TEXT
		GUI.Label(new Rect(0, 0, WIDTH, HEIGHT), line.text);
		
		//CHOICES
		int l = line.choices.Length;
		if(l == 0){
			GUIChoice(DialogChoice.defaultQuit, 0);
		}
		else {
			for(int i=0; i < l; i++){
				GUIChoice(line.choices[i], l-i-1);
			}
		}
	}
	
	void GUIChoice(DialogChoice choice, int pos){
		if(GUI.Button(new Rect(0, HEIGHT-MARGIN*5-15-pos*35, WIDTH-MARGIN*2, 30), choice.text)){			
			if(choice.gotoLine == -1){
				Close();
			}
			else {
				_currentLine = choice.gotoLine;
			}
		}
	}
}
