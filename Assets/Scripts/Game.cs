﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour{
	public static Game instance;
	
	public bool pause = false;	
	public Board board;
	
	public float timer = 10f;
	
	void Start(){
		//board.game = this;
		
		instance = this;
	}
	
	void Update(){
		if(!pause) UpdateGame();
		
		if(Input.GetKeyDown(KeyCode.Escape)){
			if(!pause)
				pause = true;
			else
				Application.LoadLevel("mainMenu");
		}
	}
	
	void UpdateGame(){
		timer -= Time.deltaTime;
	}
	
	void OnGUI(){
		GUI.skin = GUIManager.Get().guiSkin;
		GUI.Label(new Rect(5,5, 100, 100), timer.ToString("#.0"), GUI.skin.GetStyle("CounterLabel"));
		
		if(pause)
			GUI.ModalWindow(1, new Rect(-10, -10, Screen.width+20, Screen.height+20), GUIMenuWin, "");
	}
	
	void GUIMenuWin(int index){
		int width = 200;
		GUI.BeginGroup(new Rect((Screen.width - width)*0.5f, 100, width, 200));
		if(GUI.Button(new Rect(0, 0, width, 50), "Continue"))
			pause = false;
		
		if(GUI.Button(new Rect(0, 60, width, 50), "Go To Main Menu"))
			Application.LoadLevel("mainMenu");
		
		GUI.EndGroup();
	}

}
