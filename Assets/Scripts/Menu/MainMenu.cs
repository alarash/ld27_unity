﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}
	}
	
	void OnGUI(){
		GUI.skin = GUIManager.Get().guiSkin;
		
		GUI.Label(new Rect(0, 0, Screen.width, 50), "10 Sec Hero", GUI.skin.GetStyle("CounterLabel"));
		
		if(GUI.Button(new Rect(5, Screen.height - 64 - 5, Screen.width - 10, 64), "New Game")){
			Application.LoadLevel("test01");
		}
	}
}
