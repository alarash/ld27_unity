﻿using UnityEngine;
using System.Collections;
using System;

/*
public class Metronome
    {
        public event TickHandler Tick;
        public EventArgs e = null;
        public delegate void TickHandler(Metronome m, EventArgs e);
        public void Start()
        {
            while (true)
            {
                System.Threading.Thread.Sleep(3000);
                if (Tick != null)
                {
                    Tick(this, e);
                }
            }
        }
    }
        public class Listener
        {
            public void Subscribe(Metronome m)
            {
                m.Tick += new Metronome.TickHandler(HeardIt);
            }
            private void HeardIt(Metronome m, EventArgs e)
            {
                System.Console.WriteLine("HEARD IT");
            }

        }
    class Test
    {
        static void Main()
        {
            Metronome m = new Metronome();
            Listener l = new Listener();
            l.Subscribe(m);
            m.Start();
        }
    } 
*/

public class TestArgs: EventArgs {
	public string name;
	
	public TestArgs(string name){
		this.name = name;
	}
	
	override public string ToString(){
		return base.ToString()+" "+this.name;
	}
}

[Serializable]
public class TestBase {
	public delegate void Handler(TestBase t, TestArgs e);
	public event Handler handler;
	
	[SerializeField]
	private float _val = 0;
	public float val {
		get {return _val;}
		set {
			Debug.Log("test");
			_val = value;
			handler(this, new TestArgs("val changed"));
		}
	}

	/*
	public void Test(){
		handler(this, new TestArgs("YOP"));
	}*/
}

public class Test: MonoBehaviour {
	public TestBase test;
	
	void Start(){
		//SetTest(new TestBase());
		SetTest(test);
	}
	
	public void SetTest(TestBase t){
		test = t;
		test.handler += new TestBase.Handler(OnEvent);
	}
	
	void OnEvent(TestBase t, TestArgs e){
		Debug.Log("OnEvent "+t+" "+e);
	}
	
	//private float _timer = 0;
	void Update(){
		
		/*
		_timer += Time.deltaTime;
		if(_timer > 1){
			_timer = 0;
			test.Test();
		}*/
	}
}

//
//public class TestBase {
//	public delegate void TestHandler(TestBase t, EventArgs e);
//	
//	public event TestHandler Handler;
//	public EventArgs e = null;
//	
//	
//	/*public void AddEventListener(Func<TestBase, EventArgs> func){
//		Handler += new TestHandler(func);
//	}*/
//	
//	//public void addEventListener(object obj)
//	public void Test(){
//		EventArgs e = new EventArgs();
//		Handler(this, e);
//	}
//}
//
//public class Test : MonoBehaviour {
//	public TestBase t;
//	
//	private float _timer = 0;
//	
//	void Start(){
//		t = new TestBase();
//		t.Handler += new TestBase.TestHandler(OnEvent);
//		//t.AddEventListener(OnEvent);
//	}
//	
//	public void OnEvent(TestBase t, EventArgs e){
//		Debug.Log("OnEvent "+t+" "+e);
//	}
//	
//	void Update(){
//		_timer += Time.deltaTime;
//		if(_timer > 1){
//			_timer = 0;
//			t.Test();
//		}
//	}
//}
