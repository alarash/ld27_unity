using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Board : MonoBehaviour {
	public static Board instance;
	
	private List<Tile> _tiles;
	private List<Unit> _units;
	public Player player;
	
	//==========================================================================
	void Start(){
		instance = this;
		
		//TILES
		_tiles = new List<Tile>();
		foreach(Tile t in GetComponentsInChildren<Tile>()){
			_tiles.Add(t);
			//t.board = this;
			
			//t.Init();
		}
		
		foreach(Tile t in _tiles)
			t.InitNeighbours();
		
		//UNITS
		_units = new List<Unit>();
		foreach(Unit u in GetComponentsInChildren<Unit>()){
			_units.Add(u);
			//u.board = this;
			
			Player p = u.GetComponent<Player>();
			if(p) player = p;
			
			//u.Init();
		}
		
		Camera.main.GetComponent<CameraFollow>().target = player.transform;
	/*}
	
	void Start(){*/
		//ShowSelectionTileNeighbour(player.unit.tile);
		
		foreach(Unit u in _units){
			//u.Move(u.tile);
			u.tile.SendMessage("OnEnter", u, SendMessageOptions.DontRequireReceiver);
		}
		
		ShowSelectionTileNeighbour(player.unit.tile);
	}
	
	//==========================================================================
	//GETTERS
	public Tile GetTile(int x, int y){
		foreach(Tile t in _tiles){
			if(t.x == x && t.y == y)
				return t;
		}
		return null;
	}
	
	public List<Unit> GetUnits(int x, int y){
		return GetUnitsOnTile(GetTile(x, y));
	}
	
	public List<Unit> GetUnitsOnTile(Tile t){
		List<Unit> units = new List<Unit>();
		if(t == null) return units;
		foreach(Unit u in _units){
			if(u.tile == t)
				units.Add(u);
		}
		return units;
	}
	
	//==========================================================================
	public void ShowSelectionTileNeighbour(Tile tile){
		foreach(Tile t in _tiles)
			t.SetSelectVisible(tile.neighbours.Contains(t));
	}
	
	public void HideSelection(){
		foreach(Tile t in _tiles){
			t.SetSelectVisible(false);
		}
	}
	
	//==========================================================================
	public void RemoveUnit(Unit unit){
		_units.Remove(unit);
	}
	
	
	/*
	
	private List<Tile> _tiles;
	private List<Unit> _units;
	private Player _player;
	

	void Start () {
		instance = this;
		
		
		//TILES
		_tiles = new List<Tile>();
		foreach(Tile t in GetComponentsInChildren<Tile>()){
			_tiles.Add(t);
			t.board = this;
			
			t.Init();
		}
		
		//UNITS
		_units = new List<Unit>();
		foreach(Unit u in GetComponentsInChildren<Unit>()){
			_units.Add(u);
			u.board = this;
			
			Player p = u.GetComponent<Player>();
			if(p) _player = p;
			
			u.Init();
		}
		
		Camera.main.GetComponent<CameraFollow>().target = _player.transform;
		
		ShowCanMove(_player.unit);
		
		
	}
	
	public Player GetPlayer(){
		return _player;
	}
	
	public void ShowCanMove(Unit unit){
		List<Tile> neighbours = unit.GetTile().GetNeighbours();
		foreach(Tile t in _tiles){
			t.SetSelection(neighbours.Contains(t));
		}
	}
	
	public void HideSelection(){
		foreach(Tile t in _tiles){
			t.SetSelection(false);
		}
	}
	
	public Tile GetTile(int x, int y){
		foreach(Tile t in _tiles){
			if(t.x == x && t.y == y)
				return t;
		}
		return null;
	}
	
	public List<Unit> GetOtherUnits(Unit unit){
		List<Unit> units = new List<Unit>();
		foreach(Unit u in _units){
			if(unit == u) continue;
			if(unit.position.x == u.position.x && unit.position.y == u.position.y)
				units.Add(u);
		}
		return units;
	}
	
	public List<Unit> GetUnits(Tile tile){
		List<Unit> units = new List<Unit>();
		foreach(Unit u in _units){
			if(u.position.x == tile.x && u.position.y == tile.y)
				units.Add(u);
		}
		return units;
	}
	
	public void RemoveUnit(Unit unit){
		_units.Remove(unit);
	}
	
	public void OnSelect(Tile tile){
		HideSelection();
		_player.unit.MoveToTile(tile);
	}
	
	*/


}