﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour {
	
	public int x;
	public int y;
	
	//[SerializeField]
	public List<Tile> neighbours;
	//public Tile[] neighbours;
	
	private GameObject _tileSelection;
	
	//===================================================================
	void Awake(){
		x = (int)transform.localPosition.x;
		y = (int)transform.localPosition.z;
	/*}
	
	void Awake(){*/
		_tileSelection = transform.FindChild("TileSelection").gameObject;
	}
		
	public void InitNeighbours(){
		Board board = Board.instance;
		Tile tile;
		//int i = 0;
		
		//Debug.Log(x+" "+y);
		
		tile = board.GetTile(x-1, y);
		if(tile) neighbours.Add(tile);
		
		tile = board.GetTile(x+1, y);
		if(tile) neighbours.Add(tile);
		
		tile = board.GetTile(x, y-1);
		if(tile) neighbours.Add(tile);
		
		tile = board.GetTile(x, y+1);
		if(tile) neighbours.Add(tile);
		
	}
	
	//===================================================================
	public List<Unit> GetUnits(){
		return Board.instance.GetUnitsOnTile(this);
	}
	
	//===================================================================
	public void GetPos(int position, Unit u, ref Vector3 pos, ref Quaternion rot){
		
		switch(position){
		case 0:
			pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			if(this != u.prevTile && u.prevTile != null)
				rot = Quaternion.LookRotation(pos - new Vector3(u.prevTile.transform.position.x, pos.y, u.prevTile.transform.position.z));
			else rot = Quaternion.identity;
			break;
			
		case 1:
			pos = new Vector3(transform.position.x - 0.25f, transform.position.y, transform.position.z - 0.25f);
			rot = Quaternion.Euler(new Vector3(0, 45, 0));
			break;
			
		case 2:
			pos = new Vector3(transform.position.x + 0.25f,	transform.position.y, transform.position.z + 0.25f);
			rot = Quaternion.Euler(new Vector3(0, 45, 0));
			break;
		}
	}
	
	public void SetSelectVisible(bool val){
		_tileSelection.SetActive(val);
	}
	
	void OnMouseDown(){
		if(!_tileSelection.activeSelf) return;
		//board.OnSelect(this);
		Player.instance.OnTileSelected(this);
	}
	
	public void OnEnter(Unit unit){
		foreach(Unit u in GetUnits())
			if(unit != u) u.SendMessage("OnEnter", unit, SendMessageOptions.DontRequireReceiver);
	}
	
	public void OnStay(Unit unit){
		
	}
	
	public void OnLeave(Unit unit){
		foreach(Unit u in GetUnits())
			if(unit != u) u.SendMessage("OnLeave", unit, SendMessageOptions.DontRequireReceiver);
	}
	
	public bool HasBlock(){
		foreach(Unit unit in GetUnits()){
			if(unit.block)
				return true;
		}
		return false;
	}
	
	
	
	/*
	[HideInInspector]
	public Board board;
	
	public int x;
	public int y;
	
	private GameObject _tileFace;
	private GameObject _tileSelection;
	
	//public List<GameObject> neighbours;

	// Use this for initialization
	public void Init () {
		_tileFace = transform.FindChild("TileFace").gameObject;
		_tileSelection = transform.FindChild("TileSelection").gameObject;
		
		//_tileSelection.SetActive(false);
		//SetSelection(false);
		
		_tileFace.renderer.material = new Material(_tileFace.renderer.material);
		//_tileFace.renderer.material.color = new Color(Random.value, Random.value, Random.value);

		x = (int)transform.localPosition.x;
		y = (int)transform.localPosition.z;
	}
	
	public override string ToString()
	{
		return base.ToString() + ": " + x.ToString()+ ", "+ y.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown(){
		if(!_tileSelection.activeSelf) return;
		board.OnSelect(this);
	}
	
	public void SetSelection(bool val){
		//Debug.Log("SetSelection" + this+" "+val);
		_tileSelection.SetActive(val);
	}
	
	public List<Tile> GetNeighbours(){
		List<Tile> result = new List<Tile>();
		
		Tile tile = board.GetTile(x, y + 1);
		if(tile) result.Add(tile);
		
		tile = board.GetTile(x, y - 1);
		if(tile) result.Add(tile);
		
		tile = board.GetTile(x + 1, y);
		if(tile) result.Add(tile);
		
		tile = board.GetTile(x - 1, y);
		if(tile) result.Add(tile);
		
		//Debug.Log(result);
		
		return result;
	}
	*/
}
