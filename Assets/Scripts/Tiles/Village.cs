﻿using UnityEngine;
using System.Collections;

public class Village : MonoBehaviour {
	
	//private float _testTimer;
	
	private float _timer;
	
	public void OnStay(Unit unit){
		//Debug.Log(this+" onStay "+unit);
		
		_timer += Time.deltaTime;
		if(_timer > 1){
			_timer = 0;
			//unit.HP ++;
			
			//unit.Heal(this, 1);
			//unit.SendMessage("OnHeal", new Msg(this, 1), SendMessageOptions.DontRequireReceiver);
			//Msg.Send(unit, "OnHeal", new object[]{this, 1});
			//Msg.Send(unit, "OnHeal", this, )
			
			unit.SendMessage("OnHeal", new AmountMsg(this, 1), SendMessageOptions.DontRequireReceiver);
			
			
		}
		
		//Debug.Log(_testTimer);
		/*_testTimer += Time.deltaTime;
		if(_testTimer > 2 && !unit.canMove){
			unit.canMove = true;
		}*/
		
	}
	
	public void OnEnter(Unit unit){
		//Debug.Log(this+" onEnter "+unit);
		
		_timer = 0;
		
		/*_testTimer = 0;
		unit.canMove = false;*/
	}
	
	public void OnLeave(Unit unit){
		//Debug.Log(this+" onLeave "+unit);
	}
}
