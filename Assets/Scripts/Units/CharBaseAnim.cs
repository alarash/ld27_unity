﻿using UnityEngine;
using System.Collections;

public class CharBaseAnim : MonoBehaviour {
	
	private Animator _anim;
	
	static int Encounter = Animator.StringToHash("Encounter");
	static int Attack = Animator.StringToHash("Attack");
	static int Hurt = Animator.StringToHash("Hurt");
	static int Die = Animator.StringToHash("Die");
	static int Run = Animator.StringToHash("Run");
	
	static int AttackState = Animator.StringToHash("Base Layer.Attack");
	static int HurtState = Animator.StringToHash("Base Layer.Hurt");
	static int RunState = Animator.StringToHash("Base Layer.Run");

	// Use this for initialization
	void Start () {
		//_anim = GetComponent<Animator>();
		_anim = GetComponentInChildren<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
		AnimatorStateInfo animStateInfo = _anim.GetCurrentAnimatorStateInfo(0);
		/*switch(animStateInfo.nameHash){
		case AttackState:
			Debug.Log("Attack!!!");
		}*/
		if(animStateInfo.nameHash == AttackState){
			//Debug.Log("Attack State!!!");
			_anim.SetBool(Attack, false);
		}
		else if(animStateInfo.nameHash == HurtState){
			_anim.SetBool(Hurt, false);
		}
		else if(animStateInfo.nameHash == RunState){
			_anim.SetBool(Run, false);
		}
	}
	
	public void SetEncounter(bool encounter){
		_anim.SetBool(Encounter, encounter);
	}
	
	//MESSAGES
	public void OnEnter(){
		_anim.SetBool(Encounter, true);
	}
	public void OnLeave(){
		//Debug.Log(this+" OnLeave");
		_anim.SetBool(Encounter, false);
	}
	
	public void OnAttack(){
		_anim.SetBool(Attack, true);
	}
	
	public void OnHurt(){
		_anim.SetBool(Hurt, true);
	}
	
	public void OnDie(){
		_anim.SetBool(Die, true);
	}
	
	public void OnMove(){
		//OnBye();
		//Debug.Log(this+" OnMove");
		_anim.SetBool(Run, true);
	}
}
