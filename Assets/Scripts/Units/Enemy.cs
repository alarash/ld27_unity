﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	
	public Unit unit;
	public Health health;
	
	
	
	private float timer = 0f;
	private bool metPlayer = false;
	
	void Awake () {		
		unit = GetComponent<Unit>();
		health = GetComponent<Health>();
	}

	public void OnEnter(Unit unit){
		if(unit.GetComponent<Player>()){
			//Debug.Log("Attack !!!!");
			
			//unit.canMove = false;
			
			metPlayer = true;
		}
	}
	
	public void OnDie(){
		//Player.instance.unit.canMove = true;
	}
	
	void Update(){
		if(health.val <= 0 || !metPlayer || Player.instance.unit.health.val <=0) return;
		//if(unit.HP < 1 || !metPlayer || Player.instance.unit.HP < 1) return;
		timer += Time.deltaTime;
		
		if(timer > 1f){
			timer = 0f;
			
			SendMessage("OnAttack", this, SendMessageOptions.DontRequireReceiver);
			//Player.instance.unit.Hurt(unit, 1);
			//Msg.Send(Player.instance, "OnHurt", this, 1);
			Player.instance.SendMessage("OnHurt", new AmountMsg(this, 1));
			//unit.charAnim.SetAttack();
			//Player.instance.unit.Hurt(1);
		}
	}
	
	/*
	void OnGUI(){
		if(Player.instance.unit.tile == unit.tile){
			unit.GaugeGUI(Screen.width - 205, 410);
		}
	}*/
}
