﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Item {
	
	protected Unit owner;
	
	public float reloadTime = 0.5f;
	public float timer = 0f;
	
	public string name = "Item";

	public Item(Unit owner){
		this.owner = owner;
	}
	
	virtual public bool CanUse(){
		//if(owner.HP <= 0) return false;
		return timer == 0f;
	}
	
	virtual public void Use(){
		timer = reloadTime;
	}
	
	virtual public void Update(){
		if(timer > 0f){
			timer -= Time.deltaTime;
			if(timer < 0f)
				timer = 0f;
		}
			
	}
	
	virtual public void Button(int x, int y){
		Rect rect = new Rect(x, y, 100,100);
		if(CanUse()){
			if(GUI.Button(rect, name)){
				Use();
			}
		}
		else {
			GUI.Box(rect, name, GUIManager.instance.guiSkin.GetStyle("Button"));
			
			float ratio = timer / reloadTime;
			Rect fillRect = new Rect(x, y, 100, Mathf.Max(0, 100 * ratio));
			GUI.Box(fillRect, "", GUIManager.instance.guiSkin.GetStyle("ItemTimer"));
			
		}
	}
	
}

public class Weapon: Item {
	public int damage = 1;
	
	public Weapon(Unit owner):
	base(owner)
	{
		name = "Weapon";
	}
	
	override public void Use(){
		//owner.charAnim.SetAttack();
		
		owner.SendMessage("OnAttack", owner, SendMessageOptions.DontRequireReceiver);
		
		base.Use();
		
		
		Unit encounter = owner.GetEncounter();
		if(encounter != null)
			//encounter.Hurt(owner, damage);
			//encounter.SendMessage("OnHurt", new Msg(this, damage), SendMessageOptions.DontRequireReceiver);
			//Msg.Send(encounter, "OnHurt", new AmountMsg(this, damage));
			encounter.SendMessage("OnHurt", new AmountMsg(this, damage), SendMessageOptions.DontRequireReceiver);
		
		
	}
}
