﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {
	public static Player instance;
	
	[HideInInspector]
	public Unit unit;
	
	//private AudioSource audio;
	
	public AudioClip punchSound;
	public AudioClip hurtSound;
	
	void Awake(){
		instance = this;
		
		unit = GetComponent<Unit>();
		unit.block = false;
		
		unit.items = new List<Item>();
		//unit.items.Add(new Item(unit));
		unit.items.Add(new Weapon(unit));
		
		//audio = GetComponent<AudioSource>();
	}
	
	void Start(){
		//Board.instance.ShowSelectionTileNeighbour(unit.tile);
	}
	
	public void OnTileSelected(Tile t){
		Board.instance.HideSelection();
		unit.Move(t);
		//Board.instance.ShowSelectionTileNeighbour(unit.tile);
		
		
	}
	
	void OnGUI(){
		GUI.skin = GUIManager.Get().guiSkin;
		
		if(unit.health.val > 0){
			int y = 90;
			for(int i = 0; i < unit.items.Count; i++){
				unit.items[i].Button(5, y);
				y+= 105;
			}
		}
		
		//unit.GaugeGUI(5, 410);
	}
	
	void OnAttack(){
		/*audio.clip = punchSound;
		audio.Play();*/
	}
	
	void OnHurt(){
		Debug.Log("outch");
		
		//audio.clip = Resources.Load("hurt") as AudioClip;
		
		/*audio.clip = hurtSound;
		audio.Play();*/
	}
}
