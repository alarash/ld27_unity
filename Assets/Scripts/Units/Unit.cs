﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour {
	//Position
	[HideInInspector]
	public Tile prevTile;
	public Tile tile;
	
	private bool _hasTarget = false;
	private Vector3 _target;
	private Quaternion _targetQ;
	private Vector3 _velocity;
	
	private bool _isPlayer;
	
	//Attribute
	[HideInInspector]
	public Health health;
	/*
	public int HP = 10;
	public int maxHP = 10;*/
	
	/*public int MP = 0;
	public int maxMP = 0;*/
	
	//public bool block = true;
	
	
	private bool _block = true;
	public bool block {
		get {return _block;}
		set {
			_block = value;
			if(Player.instance.unit.tile == tile){
				if(!Board.instance) return;
				if(!tile.HasBlock())
					Board.instance.ShowSelectionTileNeighbour(tile);
			}
				
		}
	}
	//public bool block = true;
	
	public List<Item> items;
	
	
	// Use this for initialization
	void Start () {
		//tile.SendMessage("OnEnter", this, SendMessageOptions.DontRequireReceiver);
		
		if(items == null) items = new List<Item>();
		
		_isPlayer = GetComponent<Player>() != null;
		
		health = GetComponent<Health>();
		
		//if(tile == null) tile = Board.instance.GetTile((int)transform.position.x, (int)transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		if(tile == null){
			if(Board.instance)
				tile = Board.instance.GetTile((int)transform.position.x, (int)transform.position.z);
		}
		
		//MOVE
		if(_hasTarget){
			transform.position = Vector3.SmoothDamp(transform.position, _target, ref _velocity, 0.1f);
			transform.rotation = Quaternion.Slerp(transform.rotation, _targetQ, 0.15f);
			if(transform.position == _target){
				_hasTarget = false;
			}		
		}
		
		tile.SendMessage("OnStay", this, SendMessageOptions.DontRequireReceiver);
		
		foreach(Item item in items)
			item.Update();
	}
	
	
	//===========================================================================
	//HP Modifiers
	/*
	public void Hurt(Object sender, int amount){
		HP -= amount;
		SendMessage("OnHurt", this, SendMessageOptions.DontRequireReceiver);
		if(HP <= 0){
			HP = 0;
			SendMessage("OnDie", this, SendMessageOptions.DontRequireReceiver);
		}
		
		RisingText.Create(transform.position, "-"+amount.ToString(), Color.red);
	}
	
	public void Heal(Object sender, int amount){
		if(HP == maxHP) return;
		
		RisingText.Create(transform.position, "+"+amount.ToString(), Color.green);
		SendMessage("OnHeal", this, SendMessageOptions.DontRequireReceiver);
		
		HP += amount;
		if(HP > maxHP){
			HP = maxHP;
		}
	}*/
	
	//===========================================================================
	//MOVE
	public bool IsMoving(){
		return _hasTarget;
	}
	
	public void Move(Tile t){
		prevTile = tile;
		tile = t;
		
		/*
		if(_isPlayer && prevTile != tile){
			prevTile.SendMessage("OnLeave", this, SendMessageOptions.DontRequireReceiver);
			
			SendMessage("OnBye", this, SendMessageOptions.DontRequireReceiver);
			
			tile.SendMessage("OnEnter", this, SendMessageOptions.DontRequireReceiver);
		}
		
		List<Unit> units = t.GetUnits();
		if(units.Count == 1){
			MoveToPos(t, 0);
		}
		else {
			
			if(_isPlayer){
				MoveToPos(t, 1);
				SendMessage("OnHello", this, SendMessageOptions.DontRequireReceiver);
			}
			else {
				MoveToPos(t, 2);
			}
		}*/
		
		if(prevTile != tile)
			prevTile.SendMessage("OnLeave", this, SendMessageOptions.DontRequireReceiver);
		
		/*prevTile = tile;
		tile = t;*/
		
		List<Unit> units = t.GetUnits();
		if(units.Count == 1){
			MoveToPos(t, 0);
			SendMessage("OnLeave", this, SendMessageOptions.DontRequireReceiver);
			SendMessage("OnMove", this, SendMessageOptions.DontRequireReceiver);
		}
		else {
			SendMessage("OnEnter", this, SendMessageOptions.DontRequireReceiver);
			if(_isPlayer){
				MoveToPos(t, 1);
				//SendMessage("OnEnter", this, SendMessageOptions.DontRequireReceiver);
			}
			else {
				MoveToPos(t, 2);
			}
		}
		
		if(prevTile != tile)
			tile.SendMessage("OnEnter", this, SendMessageOptions.DontRequireReceiver);
		
		if(_isPlayer){
			if(!tile.HasBlock())
				Board.instance.ShowSelectionTileNeighbour(tile);
		}
	}
	
	public void MoveToPos(Tile t, int position){
		t.GetPos(position, this, ref _target, ref _targetQ);
		_hasTarget = true;
		//SendMessage("OnMove", this, SendMessageOptions.DontRequireReceiver);
	}
	
	//===========================================================================
	//ENCOUNTERS
	
	public List<Unit> GetEncounters(){
		List<Unit> units = tile.GetUnits();
		units.Remove(this);
		return units;
	}
	
	public Unit GetEncounter(){
		List<Unit> units = GetEncounters();
		if(units.Count > 0)
			return units[0];
		return null;
	}
	
	//===========================================================================
	//MESSAGE
	public void OnEnter(Unit unit){
		if(!_isPlayer)
			MoveToPos(tile, 2);
	}
	
	public void OnLeave(Unit unit){
		if(!_isPlayer)
			MoveToPos(tile, 0);
	}
	
	public void OnDie(){		
		Destroy(gameObject, 1f);
		Board.instance.RemoveUnit(this);
		
		if(!_isPlayer && tile == Player.instance.unit.tile && block){
			if(!tile.HasBlock())
				Board.instance.ShowSelectionTileNeighbour(tile);
		}
	}
	
	//===========================================================================
	//GUI
	
	/*
	public void GaugeGUI(float x, float y){
		
		GUIManager.Get().Gauge(new Rect(x, y, 200, 32), "HP", HP, maxHP);
		GUIManager.Get().Gauge(new Rect(x, y + 35, 200, 32), "MP", MP, maxMP);
	}
	
	void OnGUI(){
		if(_isPlayer)
			GaugeGUI(5, 410);
		else if(tile == Player.instance.unit.tile)
			GaugeGUI(Screen.width - 205, 410);
	}*/
	
	void OnGUI(){
		int l = (health != null?1:0);// + (health != null?1:0);
		
		if(health){
			if(_isPlayer)
				health.GaugeGUI(5, Screen.height - l * 40);
			else if(tile == Player.instance.unit.tile)
				health.GaugeGUI(Screen.width - 205, Screen.height - l * 35);
			
			l--;
		}
		
		/*
		if(health){
			if(_isPlayer)
				health.GaugeGUI(5, Screen.height - l * 40);
			else if(tile == Player.instance.unit.tile)
				health.GaugeGUI(Screen.width - 205, Screen.height - l * 35);
			
			l--;
		}*/
			
			
	}
	
}
