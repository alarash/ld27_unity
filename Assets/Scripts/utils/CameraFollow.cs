﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	
	public Transform target;
	public Vector3 offset;
	public float dampTime = 0.3f;
	
	private Vector3 velocity = Vector3.zero;
	
	// Update is called once per frame
	void Update () {
		
		if(target){
			Vector3 pos = target.GetComponent<Unit>().tile.transform.position;
			transform.position = Vector3.SmoothDamp(
				transform.position,
				offset + pos,
				ref velocity,
				dampTime
			);
			/*
			transform.position = Vector3.SmoothDamp(
				transform.position,
				offset + target.transform.position,
				ref velocity,
				dampTime
			);*/
		}
		
		/*
		if (target)
		{
			Vector3 point = camera.WorldToViewportPoint(target.position);
			Vector3 delta = target.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
		}*/
	}
}


