﻿using UnityEngine;
using System.Collections;


public class GUIManager : MonoBehaviour {
	public static GUIManager instance;
	
	public GUISkin guiSkin;
	
	void Start(){
		instance = this;
	}
	
	public static GUIManager Get(){
		//return GameObject.Find("_GUI").GetComponent<GUIManager>();
		return instance;
	}
	
	
	
	void OnGUI(){
		GUI.skin = guiSkin;
		/*
		GUI.Label(new Rect(5,5, 100, 100), "10.0", GUI.skin.GetStyle("CounterLabel"));
		
		int y = 100;
		GUI.Button(new Rect(5, y, 100, 100), "Sword");
		GUI.Button(new Rect(5, y + 105, 100, 100), "Sword");
		GUI.Button(new Rect(5, y + 210, 100, 100), "Sword");*/
	}
	
	public void Gauge(Rect r, string label, int val, int maxVal){
		GUI.skin = guiSkin;
		GUI.Box(r, "", GUI.skin.GetStyle("Button"));
		r.x += 5;
		GUI.Label(r, label+" :"+val+"/"+maxVal);
	}
		
}
