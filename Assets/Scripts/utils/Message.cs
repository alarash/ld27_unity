﻿using UnityEngine;
using System.Collections;


public class Msg {
	public object sender;
	
	public Msg(object sender){
		this.sender = sender;
	}
	
	public Msg()
	:this(null)
	{}
}

public class AmountMsg: Msg {
	public int amount;
	
	public AmountMsg(object sender, int amount)
	:base(sender)
	{
		this.amount = amount;
	}
	
	public AmountMsg(int amount)
	:this(null, amount)
	{}
	
}
