﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TextMesh))]
public class RisingText : MonoBehaviour
{
	public Color color = Color.white;
	public float speed = 1f;
	public float alphaSpeed = 0.5f;
	
	float alpha = 1f;
	
	void Start(){
		
		
	}
	
	void Update(){
		transform.Translate(Vector3.up * speed, Space.World);
		//transform.LookAt(Camera.main.transform.position);
		transform.rotation = Camera.main.transform.rotation;
		
		alpha -= Time.deltaTime * alphaSpeed;
		if(alpha <= 0f) Destroy(gameObject);
		
		renderer.material.color = new Color(color.r, color.g, color.b, alpha);
		
		
	}
	
	public static void Create(Vector3 position, string label, Color color){
		GameObject go = Instantiate(Resources.Load("RisingText"), position, Quaternion.identity) as GameObject;
		go.GetComponent<TextMesh>().text = label;
		go.GetComponent<RisingText>().color = color;
	}
	
	/*
	// private variables:
	Vector3 crds_delta;
	float   alpha;
	float   life_loss;
	Camera  cam;
	
	// public variables - you can change this in Inspector if you need to
	public Color color = Color.white;
	
	// SETUP - call this once after having created the object, to make it 
	// "points" shows the points.
	// "duration" is the lifespan of the object
	// "rise speed" is how fast it will rise over time.
	public void setup(int points, float duration, float rise_speed)
	{
	    GetComponent<TextMesh>().text = points.ToString();     
	    life_loss = 1f / duration;
	    crds_delta = new Vector3(0f, rise_speed, 0f);   
	}
	
	void Start() // some default values. You still need to call "setup"
	{
	    alpha = 1f;
	    cam = Camera.main;
	    crds_delta = new Vector3(0f, 1f, 0f);
	    life_loss = 0.5f;
	}
	
	void Update () 
	{
	    // move upwards :
	    transform.Translate(crds_delta * Time.deltaTime, Space.World);
	
	    // change alpha :
	    alpha -= Time.deltaTime * life_loss;
	    renderer.material.color = new Color(color.r,color.g,color.b,alpha);
	
	    // if completely faded out, die:
	     if (alpha <= 0f) Destroy(gameObject);
	
	    // make it face the camera:
	    transform.LookAt(cam.transform.position);
	    transform.rotation = cam.transform.rotation;     
	}
	*/
}